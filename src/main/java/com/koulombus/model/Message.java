package com.koulombus.model;

import java.time.LocalDateTime;

public record Message(String message, LocalDateTime createdAt) {

}
