package com.koulombus.model;

public record MessageRequest(String message) {
}
